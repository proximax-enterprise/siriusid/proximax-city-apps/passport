const fs = require('fs');

function replaceFile(filePath, searchString, replaceString) {
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(searchString, replaceString);

        fs.writeFile(filePath, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

const f = 'node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js';
replaceFile(f, /node: false/g, "node: {fs: 'empty', global: true, crypto: 'empty', tls: 'empty', net: 'empty', process: true, module: false, clearImmediate: false, setImmediate: false}");

f_crypto = 'node_modules/tsjs-xpx-chain-sdk/dist/src/core/crypto/Crypto.js';
replaceFile(f_crypto, 'require(\'crypto\');', 'require(\'crypto-browserify\');');

f_codec = 'node_modules/cids/src/index.js';
replaceFile(f_codec, 'require(\'multicodec/src/base-table\')', 'require(\'multicodec/src/base-table.json\')');

f_nemkey = 'node_modules/tsjs-chain-xipfs-sdk/build/main/src/lib/cipher/nem-keys-decipher-stream.js';
replaceFile(f_nemkey, 'require(\"crypto\")', 'require(\'crypto-browserify\')');