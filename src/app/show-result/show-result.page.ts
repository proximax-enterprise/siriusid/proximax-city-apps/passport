import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage implements OnInit {

  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  motherName;
  fatherName;
  passportID;
  passportType;
  allowEntry;
  expiryDate;
  height;
  eyeColor;
  married;
  spouse;
  plan;
  country;
  emergencyContact;
  client_name;

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
  ) {
    this.client_name = this.clientInfo.credential[0].content[0][1] + " " + this.clientInfo.credential[0].content[1][1];
    let content = this.clientInfo.credential[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];
      this.motherName = content[6][1];
      this.fatherName = content[7][1];
      this.passportID =  content[8][1];
      this.passportType = content[9][1];
      // this.allowEntry = content[10][1];
      this.expiryDate =content[10][1];
      this.height = content[11][1];
      this.eyeColor = content[12][1];
      this.married = content[13][1];
      this.spouse = content[14][1];
      // this.plan = content[16][1];
      // this.country = content[17][1];
      // this.emergencyContact = content[18][1];
   }



  ngOnInit() {
  }

  checkResult(param: string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

}
