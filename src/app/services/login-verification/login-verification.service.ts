import { Injectable } from '@angular/core';
import { AdditionStored, ApiNode, CredentialRequired, CredentialStored } from 'siriusid-sdk';
import { EncryptedMessage, PublicAccount, TransferTransaction } from 'tsjs-xpx-chain-sdk';
import { StorageBucketService } from '../storage-bucket/storage-bucket.service';

@Injectable({
  providedIn: 'root'
})
export class LoginVerificationService {

  message;
  credentials;
  addition;

  constructor(
    private storageBucketService: StorageBucketService
  ) { }

  verify(transactionReceived: any, sessionToken: string, credentials: CredentialRequired[], privateKey: string, addition?: any) {
    const transferTx = transactionReceived as TransferTransaction;
    const senderPublicKey = transferTx.signer.publicKey;
    const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
    const plainMessage = EncryptedMessage.decrypt(transferTxMessage, privateKey, <PublicAccount>transferTx.signer);

    const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
    this.message = JSONtransferTxmessage;
    console.log(this.message);
    let step1 = this.verifyCredentials2(sessionToken, JSONtransferTxmessage, credentials);
    let step2 = false;
    if (addition) {
      step2 = this.verifyAddition(JSONtransferTxmessage, addition);
    }
    else step2 = true;

    if (step1 && step2) {
      return true;
    }
    else return false;

  }

  async verifyCredentials(sessionToken: string, JSONtransferTxmessage: any, senderPublicKey: string | undefined, credentials: CredentialRequired[]) {
    let credentialsReceived = new Array();
    for (let i = 0; i < JSONtransferTxmessage.payload.credentials.length; i++) {
      // the download part...
      let {fileKey, decryptKey} = JSONtransferTxmessage.payload.credentials[i]
      let credential = await this.getCredential(fileKey, decryptKey, senderPublicKey);
      
      credentialsReceived.push(credential);
    }
    console.log(credentialsReceived, credentials);
    let credentials_req = this.findMapArray(credentialsReceived, credentials);
    console.log(credentials_req);
    //Check if message received is a LOGIN message
    if (JSONtransferTxmessage.messageType == 1) {
      if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
        && credentials_req.length == credentials.length
      ) {
        this.credentials = credentialsReceived;
        console.log("Login successfully");
        return true;
      }
      else {
        console.log("Login failed");
        return false;
      }
    }
  }

  // skipped S3
  verifyCredentials2(sessionToken: string, JSONtransferTxmessage: any, credentials: CredentialRequired[]) {
    let credentialsReceived = new Array();
    for (let i = 0; i < JSONtransferTxmessage.payload.credentials.length; i++) {      
      credentialsReceived.push(JSONtransferTxmessage.payload.credentials[i]);
    }
    console.log(credentialsReceived, credentials);
    let credentials_req = this.findMapArray(credentialsReceived, credentials);
    console.log(credentials_req);
    //Check if message received is a LOGIN message
    if (JSONtransferTxmessage.messageType == 1) {
      if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
        && credentials_req.length == credentials.length
      ) {
        this.credentials = credentialsReceived;
        console.log("Login successfully");
        return true;
      }
      else {
        console.log("Login failed");
        return false;
      }
    }
  }

  verifyAddition(JSONtransferTxmessage: any, addition: any) {
    console.log(JSONtransferTxmessage);
    // let additionStored = new AdditionStored(JSONtransferTxmessage.payload.addition['publicKey'], JSONtransferTxmessage.payload.addition['privateKey'], JSONtransferTxmessage.payload.addition['additionHash']);
    let additionReceived = JSONtransferTxmessage.payload.addition;

    console.log(additionReceived);
    if (addition.length != additionReceived.length) {
      return false;
    }

    for (let i = 0; i < addition.length; i++) {
      if (addition[i]['name'] != additionReceived[i]['name']) {
        return false;
      }
    }
    this.addition = additionReceived;
    return true;
  }








  async getCredential(fileKey: string, decryptKey: string, senderPublicKey: string) {
    const publicAccount = PublicAccount.createFromPublicKey(senderPublicKey, ApiNode.networkType);
    let encryptedCredentials = (await this.storageBucketService.getFile(fileKey)).data;
    
    const encMess = EncryptedMessage.createFromPayload(encryptedCredentials);
    let decMess = EncryptedMessage.decrypt(encMess, decryptKey, publicAccount);
    return JSON.parse(decMess.payload);
  }


  /**
   * Get array of intersection elements from 2 arrays
   * @param arr1 received array
   * @param arr2 required array
   */
  findMapArray(arr1: any[], arr2: any[]) {
    let resultArray = new Array();
    for (let i = 0; i < arr2.length; i++) {
      let idRequired = arr2[i].id;
      for (let j = 0; j < arr1.length; j++) {
        if (arr1[j]['id'] == idRequired) {
          resultArray.push(arr1[j]);
        }
      }
    }
    return resultArray;
  }



}
