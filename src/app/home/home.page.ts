import { Component} from '@angular/core';
import {LoginRequestMessage, VerifytoLogin,ApiNode, AdditionType, AdditionRequire, CredentialRequired} from 'siriusid-sdk';
import {Listener, TransferTransaction, EncryptedMessage, PublicAccount,NetworkType} from 'tsjs-xpx-chain-sdk';
import {ChangeDataService} from '../../services/change-data.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {ClientInfoService} from 'src/services/client-info.service';
import { LoginVerificationService } from '../services/login-verification/login-verification.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  linkLogin;
  linkCredential;

  img = null;
  showImg = false;
  URL;
  currentNode = "api-1.testnet2.xpxsirius.io";
  status = null;
  lived = false;
  subscribe;
  listener: Listener;
  connect = false;
  credential_id = "proximaxId";
  credential;
  alert = true;
  warning = false;
  flag = true;
  step1 = false;
  addition1;
  // addition2;

  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    public alertController: AlertController,
    private clientInfo: ClientInfoService,
    private loginVerification: LoginVerificationService
    ) {
      console.log('api node: ' + this.currentNode)
    //this.loginRequestAndVerify();
  }

  ionViewWillEnter(){
    ApiNode.apiNode = "https://"+this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if(this.flag){
      this.presentAlert2();
      this.flag = false;
    } else {
      this.step1 = false;
      this.presentAlert();
    }
    
  }
  // for QR code
  async loginRequestAndVerify(){
    this.connect = false;
    this.step1 = true;
    this.addition1 = AdditionRequire.create(AdditionType.TEXT,'country','Type your country');
    // this.addition2 = AdditionRequire.create(AdditionType.IMAGE,'selfie1','Select or take a selfie');
    // this.credential = CredentialRequired.create(this.credential_id,["First Name", "Last Name", "Phone Number"]); 
    this.credential = CredentialRequired.create(this.credential_id); 
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp,[this.credential],[this.addition1 /*,this.addition2*/]);
    const sessionToken = loginRequestMessage.getSessionToken();
    //console.log('sessionToken = ' + sessionToken);
    this.img = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    console.log(loginRequestMessage);
    
    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect){
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    
    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        console.log(transaction);  // a transfer transaction with encrypted message as its content
        console.log(this.credential); // {id: 'proximaxId', option: undefined}

        let verify = this.loginVerification.verify(transaction, sessionToken,[this.credential],this.changeDataService.privateKeydApp,[this.addition1 /*,this.addition2*/])

        if (verify){
          this.connect = true;
          console.log("Transaction matched");
          this.changeDataService.addressSiriusid = transaction.signer.address.plain();
          this.clientInfo.credential = this.loginVerification.credentials;
          this.clientInfo.addition = this.loginVerification.addition;
          this.subscribe.unsubscribe();
          this.router.navigate(['/submit-infomation']);
        }
        else console.log("Transaction not match"); 
              
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
  // login(){
  //   this.loginRequestAndVerify();
  // }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'We will ask you to share',
      message: '<div class="alert"><img style="width:10%;" src="assets/icon-credentials-employment-pass.svg" ><p>ProximaXCity ID</p></div><div class="alert"><img class="additionImg" src="assets/addition-icon.png" ><p>Country</p></div>',
      buttons: [
        {
        
          text: 'Login' ,
          cssClass: 'primary',
          handler: () => {
            this.loginRequestAndVerify();
          }
        }
      ],
      cssClass:"boxAlert1"
    });

    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      header: 'Welcome to ProximaXCity Passport',
      // message: '<div class="alert"><img src="assets/icon-credentials-employment-pass.svg"><p>ProximaXCity ID</p></div>',
      buttons: [
        {
          text: 'Create Passport' ,
          cssClass: 'primary',
          handler: () => {
            this.presentAlert();
          }
          
        },
        {
          text: 'Hard Verification' ,
          cssClass: 'primary',
          handler: () => {
            this.checkResult('hard');
            // this.router.navigate(['/check-result']);
          }
        },
        {
          text: 'Light Verification' ,
          cssClass: 'primary',
          handler: () => {
            this.checkResult('light');
            // this.router.navigate(['/check-result']);
          }
        },
      ],
      
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.connect = true;
    // this.subscribe.unsubscribe();
    this.router.navigate(['/check-result']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }

}
