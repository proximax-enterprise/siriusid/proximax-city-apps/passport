import { Component} from '@angular/core';
import {CredentialRequestMessage, Credentials} from 'siriusid-sdk';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {ChangeDataService} from '../../services/change-data.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-submit-infomation',
  templateUrl: './submit-infomation.page.html',
  styleUrls: ['./submit-infomation.page.scss'],
})
export class SubmitInfomationPage{

  img = null;
  URL;
  status = null;
  lived = false;
  subscribe;
  flag = true;
  continue = true;
  connect = false;

  linkLogin;
  linkCredential;

  /** National Information */
  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  motherName;
  fatherName;

  /** Passport Information*/
  passportID = "ML01234";
  passportType = "Ordinary Passport";
  allowEntry;
  expiryDate = "01/01/2025";
  height;
  eyeColor;
  married;
  spouse;
  plan;
  country;
  emergencyContact;
  aggree;
  // selfie;

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
    public alertController: AlertController,
    private changeDataService:ChangeDataService,
    public domSanitizer: DomSanitizer
    ) {
      let content = this.clientInfo.credential[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];
      this.motherName = content[6][1];
      this.fatherName = content[7][1];

      console.log("ehem");
      console.log(this.clientInfo.addition);
      console.log("ehem");
      this.country = this.clientInfo.addition[0].content;
      // this.selfie = this.domSanitizer.bypassSecurityTrustUrl(this.clientInfo.addition[1].value);
  }

  // for QR code
  async createCredential(){
    if (this.height && this.eyeColor && this.married && this.spouse && this.aggree){
      this.continue = true;
      let content = new Map<string,string>([
        ['First Name', this.fname],
        ['Last Name', this.lname],
        ['Date of Birth', this.birth],
        ['Address', this.address],
        ['Phone number', this.phoneNumber],
        ['Email', this.email],
        ["Mother's Name", this.motherName],
        ["Father's Name", this.fatherName],
        ['Passport ID', this.passportID],
        ['Type of passport', this.passportType],
        // ['Entry checkpoint', this.allowEntry],
        ['Expiry date', this.expiryDate],
        ['Height', this.height],
        ['Eye color', this.eyeColor],
        ['Have you married', this.married],
        ['Name of spouse', this.spouse],
        // ['Travel plan', this.plan],
        // ['Country to travel', this.country],
        // ['Emergency contact', this.emergencyContact]
      ])
      const credential = Credentials.create(
        'passport',
        'ProximaXCity Passport',
        'ProximaXCity Passport Department',
        '/assets/icon-credentials-digital-passport.svg',
        [],
        content,
        'Passport and Visa',
        Credentials.authCreate(content,this.changeDataService.privateKeydApp)

      );
      const msg = CredentialRequestMessage.create(credential);
      //console.log(msg);
  
      this.flag = false;
      this.img = await msg.generateQR();
      this.linkCredential = await msg.universalLink();
    }
    else {
      this.alertWarning();
    }

    
  }


  async alertWarning() {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Fill these informations to continue',
      buttons: [
        {
          text: 'Ok' ,
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  welcome(){
    this.clientInfo.haveCredential = true;
    this.router.navigate(['/welcome']);
  }

  ionViewWillEnter(){
    //console.log("enter");
    this.flag = true;
  }


  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
